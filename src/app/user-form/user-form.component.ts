import { Component, OnInit } from '@angular/core';
import { User } from '../models/user';
import { UserService } from '../services/user.service';

@Component({
  selector: 'app-user-form',
  templateUrl: './user-form.component.html',
  styleUrls: ['./user-form.component.css']
})
export class UserFormComponent implements OnInit {

  newUser: User = {
    DateOfBirth: null,
    Email: null,
    FirstName: null,
    Id: null,
    LastName: null,
    MobileNumber: null,
    Modified: null
  };

  constructor(private userService: UserService) { }

  ngOnInit(): void {

  }

  saveUser(): void {
    this.userService
    .addUser(this.newUser)
    .subscribe();
  }

}
