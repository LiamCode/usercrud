export interface User {
    Id: number;
    FirstName: string;
    LastName: string;
    Email: string;
    MobileNumber: string
    DateOfBirth: Date;
    Modified: Date;
}