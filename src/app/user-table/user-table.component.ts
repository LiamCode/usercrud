import { Component, OnInit } from '@angular/core';
import { UserService } from '../services/user.service';
import { User } from '../models/user';

@Component({
  selector: 'app-user-table',
  templateUrl: './user-table.component.html',
  styleUrls: ['./user-table.component.css']
})
export class UserTableComponent implements OnInit {

  users: User[];
  displayedColumns: string[] = [
    'Id', 'FirstName', 'LastName', 'Email',
    'MobileNumber', 'DateOfBirth', 'Modified'
  ]

  constructor(private userService: UserService) { }

  ngOnInit(): void {
    this.showUsers();
  }

  showUsers() {
    this.userService.getUsers()
      .subscribe((data: User[]) => {
        this.users = data;
      })
  }


}
